<?php

?><!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Повторяйка</title>
	<link rel="stylesheet" href="/style.css">
</head>
<body>
	<div class="panel">
		<button class="panel__button panel__button_add">Добавить</button>
		<button class="panel__button panel__button_repeat">Повторить</button>
	</div>
	<div id="wrapper">
		<div class="common-list"></div>
	</div>
	<div class="note"></div>
	<div class="add-form">
		<div class="add-form__close-lawyer"></div>
		<form class="add-form__form">
			<textarea rows="4" class="add-form__textarea add-form__question" placeholder="Вопрос"></textarea>
			<textarea rows="4" class="add-form__textarea add-form__answer" placeholder="Ответ, который нужно выучить"></textarea>
			<!--<label class="add-form__label" title="Нужно ответить не только на вопрос, но и на ответ ответить вопросом">
				<input type="checkbox" class="add-form__checkbox add-form__checkbox_reverse-mode">
				Реверсисный режим
			</label>
			<label class="add-form__label" title="Одна ошибка и нужно начинать все сначала">
				<input type="checkbox" class="add-form__checkbox add-form__checkbox_hard-mode">
				Строгий режим
			</label>-->
			<button class="add-form__button">Добавить</button>
		</form>
	</div>
	<div class="repeat-form">
		<div class="repeat-form__close-lawyer"></div>
		<div class="repeat-form__inner">
			<div class="repeat-form__note"></div>
			<div class="reapeat-form__question"></div>
			<div class="repeat-form__hint"></div>
			<button class="repeat-form__show-hint">Показать подсказку</button>
			<textarea rows="5" class="repeat-form__textarea repeat-form__textarea_answer"></textarea>
			<div class="repeat-form__control">
				<button class="repeat-form__button repeat-form__button_skip">Пропустить</button>
				<button class="repeat-form__button repeat-form__button_check">Проверить</button>
				<button class="repeat-form__button repeat-form__button_next">Далее</button>
			</div>
		</div>
	</div>
	<script src="/js/jquery-3.1.1.min.js"></script>
	<script src="/js/common.js"></script>
</body>
</html>
<?php
	
	include $_SERVER['DOCUMENT_ROOT'] . '/config.php';
	define('SECONDS_PER_MINUTE', 60);
	define('MINUTES_PER_HOUR', 60);
	define('HOURS_PER_DAY', 24);
	define('DAYS_PER_MONTH', 30);

	// Добавляет последовательность для запоминания
	// Аргументы:
	// $question - вопрос
	// $answer - ответ
	// $hardMode - установлен ли жесткий режим
	// $reverseMode - нужно ли запоминать и вопрос и ответ
	function addSeq($question, $answer, $hardMode = 0, $reverseMode = 0) {
		$keys = array('question', 'answer', 'hard_mode', 'reverse_mode', 'time_stamp');
		$values = array($question, $answer, $hardMode, $reverseMode, time());

		return insertQuery($keys, $values, 'items');
	}

	// Возвращает последовательности не повторенные первый раз
	function selectRepeated0() {
		$condition = "WHERE r1=0";
		$keys = array('id', 'question', 'answer', 'r1', 'r2', 'r3', 'r5', 'r6');
		$res = selectQuery($keys, 'items', NULL, $condition);
		$assoc = resToAssoc($res);

		foreach ($assoc as $k => $v) {
			$assoc[$k]['repeatNumber'] = 1;
		}

		return $assoc;
	}

	// Возвращает последовательности не повторенные второй раз
	function selectRepeated1() {
		$nextRepeatTime = time() - 20 * SECONDS_PER_MINUTE;
		$condition = "WHERE r1=1 AND r2=0 AND time_stamp<" . $nextRepeatTime;
		$keys = array('id', 'question', 'answer', 'r1', 'r2', 'r3', 'r5', 'r6');
		$res = selectQuery($keys, 'items', NULL, $condition);
		$assoc = resToAssoc($res);

		foreach ($assoc as $k => $v) {
			$assoc[$k]['repeatNumber'] = 2;
		}

		return $assoc;
	}

	// Возвращает последовательности не повторенные третий раз
	function selectRepeated2() {
		$nextRepeatTime = time() - 8 * MINUTES_PER_HOUR * SECONDS_PER_MINUTE;
		$condition = "WHERE r1=1 AND r2=1 AND r3=0 AND time_stamp<" . $nextRepeatTime;
		$keys = array('id', 'question', 'answer', 'r1', 'r2', 'r3', 'r5', 'r6');
		$res = selectQuery($keys, 'items', NULL, $condition);
		$assoc = resToAssoc($res);

		foreach ($assoc as $k => $v) {
			$assoc[$k]['repeatNumber'] = 3;
		}

		return $assoc;
	}

	// Возвращает последовательности не повторенные четвертый раз
	function selectRepeated3() {
		$nextRepeatTime = time() - HOURS_PER_DAY * MINUTES_PER_HOUR * SECONDS_PER_MINUTE;
		$condition = "WHERE r1=1 AND r2=1 AND r3=1 r4=0 AND time_stamp<" . $nextRepeatTime;
		$keys = array('id', 'question', 'answer', 'r1', 'r2', 'r3', 'r5', 'r6');
		$res = selectQuery($keys, 'items', NULL, $condition);
		$assoc = resToAssoc($res);

		foreach ($assoc as $k => $v) {
			$assoc[$k]['repeatNumber'] = 4;
		}

		return $assoc;
	}

	// Возвращает последовательности не повторенные пятый раз
	function selectRepeated4() {
		$nextRepeatTime = time() - 14 * HOURS_PER_DAY * MINUTES_PER_HOUR * SECONDS_PER_MINUTE;
		$condition = "WHERE r1=1 AND r2=1 AND r3=1 r4=0 AND r5=0 AND time_stamp<" . $nextRepeatTime;
		$keys = array('id', 'question', 'answer', 'r1', 'r2', 'r3', 'r5', 'r6');
		$res = selectQuery($keys, 'items', NULL, $condition);
		$assoc = resToAssoc($res);

		foreach ($assoc as $k => $v) {
			$assoc[$k]['repeatNumber'] = 5;
		}

		return $assoc;
	}

	// Возвращает последовательности не повторенные шестой раз
	function selectRepeated5() {
		$nextRepeatTime = time() - 2 * DAYS_PER_MONTH * HOURS_PER_DAY * MINUTES_PER_HOUR * SECONDS_PER_MINUTE;
		$condition = "WHERE r1=1 AND r2=1 AND r3=1 r4=0 AND r5=0 AND r6=0 AND time_stamp<" . $nextRepeatTime;
		$keys = array('id', 'question', 'answer', 'r1', 'r2', 'r3', 'r5', 'r6');
		$res = selectQuery($keys, 'items', NULL, $condition);
		$assoc = resToAssoc($res);

		foreach ($assoc as $k => $v) {
			$assoc[$k]['repeatNumber'] = 6;
		}

		return $assoc;
	}

	function getSeq() {
		$repeated0 = selectRepeated0();
		$repeated1 = selectRepeated1();
		$repeated2 = selectRepeated2();
		$repeated3 = selectRepeated3();
		$repeated4 = selectRepeated4();
		$repeated5 = selectRepeated5();

		return array_merge($repeated0, $repeated1, $repeated2, $repeated3, $repeated4, $repeated5);
	}

	function getAllSeq() {
		$keys = array('question', 'answer', 'r1', 'r2', 'r3', 'r5', 'r6');

		return resToAssoc(selectQuery($keys, 'items'));
	}

	// Повторяет последовательность
	// Аргументы:
	// $id - идентификатор последовательности
	// $repeatNumber - номер повторения
	function repeatSeq($id, $repeatNumber) {
		$repeatNumber = 'r' . $repeatNumber;
		$keys = array($repeatNumber, 'time_stamp');
		$values = array(1, time());

		return updateQuery($keys, $values, 'items', $id);
	}

	// Удаляет последовательность для запоминания
	// Аргументы:
	// $id -  идентификатор последовательности
	function removeSeq($id) {
		return deleteQuery('items', $id);
	}

	// Возвращает соединение к базе
	function myLink() {
		GLOBAL $db_host;
		GLOBAL $db_username;
		GLOBAL $db_password;
		GLOBAL $db_name;

		return mysqli_connect($db_host, $db_username, $db_password, $db_name);
	}

	// Экранирует небезопасные символы
	// Аргументы:
	// $arr - ассоциативный массив или строка содержащие значения для экранирования
	function escape($arr) {
		$link = myLink();

		if (is_array($arr)) {
			foreach ($arr as $key => $value) {
				if (is_array($value)) {
					$arr[$key] = escape($value);
				} else {
					$arr[$key] = "'" . mysqli_real_escape_string($link, $value) . "'";
				}
			}
		} else {
			$arr = mysqli_real_escape_string($link, $arr);
			$arr = "'" . $arr . "'";
		}

		return $arr;
	}

	// Производит вставку данных в базу
	// Аргументы:
	// $keys 	 - строка с ключем или массив ключей с названиями ключей таблицы
	// $values   - значение или массив значений
	// $table 	 - название таблицы
	function insertQuery($keys, $values, $table) {
		$link = myLink();
		$values = escape($values);
		$query = "INSERT INTO " . $table;
		$query .= " (";

		if (is_array($keys)) {
			foreach ($keys as $k => $v) {
				$keys[$k] = '`' . $v . '`';
			}

			$query .= implode(",", $keys);
		} else {
			$query .= '`' . $keys . '`';
		}

		$query .= ") VALUES (";

		if (is_array($values)) {
			$query .= implode(",", $values);
		} else {
			$query .= $values;
		}

		$query .= ");";
		$res = mysqli_query($link, $query);

		return $res;
	}

	function selectQuery($keys, $table, $id = NULL, $condition = NULL) {
		$link = myLink();

		if (is_array($keys)) {
			foreach ($keys as $k => $v) {
				$keys[$k] = '`' . $v . '`';
			}

			$query = "SELECT " . implode(",", $keys);
		} else {
			$query = "SELECT " . '`' . $keys . '`';
		}

		$query .= " FROM " . $table;

		if (!is_null($id) && is_null($condition)) {
			$query .= " WHERE id=" . $id;
		}

		if (!is_null($condition) && is_null($id)) {
			$query .= " " . $condition;
		}

		$query .= ";";
		$res = mysqli_query($link, $query);

		return $res;
	}

	// Удаляет запись из базы
	// Аргументы:
	// $table - название таблицы
	// $id    - идентификатор записи
	function deleteQuery($table, $id) {
		$id *= 1;
		$link = myLink();
		$query = "DELETE FROM ";
		$query .= $table;
		$query .= " WHERE id=" . $id . ";";
		$res = mysqli_query($link, $query);

		return $res;
	}

	// Обновляет записись в базе по идентификатору
	// Аргументы:
	// $key   - ключ массив ключей
	// $value - значение или массив значений
	// $table - название таблицы
	// $id    - идентификатор записи
	function updateQuery($keys, $values, $table, $id) {
		$values = escape($values);
		$id *= 1;
		$link = myLink();
		$keysCount = count($keys);
		$valuesCount = count($values);

		if ($keysCount != $valuesCount) {
			return false;
		} else {
			$query = "UPDATE " . $table;
			$query .= " SET ";

			for ($i = 0; $i < $keysCount; $i++) {
				$query .= $keys[$i] . "=" . $values[$i];
				if ($i < $keysCount - 1) {
					$query .= ", ";
				}
			}
			$query .= " WHERE id=" . $id . ";";
		}

		$res = mysqli_query($link, $query);

		return $res;
	}

	function resToAssoc($res) {
		$assoc = array();

		if ($res) {
			while ($row = mysqli_fetch_assoc($res)) {
				array_push($assoc, $row);
			}
		}

		return $assoc;
	}
?>
(function() {
	$('.panel__button_add').click(showAddForm);
	$('.add-form__close-lawyer').click(closeAddForm);
	$('.add-form__button').click(addSeq);
	$('.panel__button_repeat').click(showRepeatForm);
	$('.repeat-form__close-lawyer').click(closeRepeatForm);
	$('.repeat-form__button_check').click(checkSeq);
	$('.repeat-form__button_next').click(nextSeq);
	$('.repeat-form__show-hint').click(showHint);
	var seq = null;
	var currSeq = 0;

	function showAddForm() {
		$('.add-form').css('display', 'flex');
	}

	function closeAddForm() {
		$('.add-form').hide();
	}

	function addSeq(e) {
		var data = {};
		data.question = $('.add-form__question').val();
		data.answer = $('.add-form__answer').val();
		data.action = 'addSeq';
		e.preventDefault();
		$.ajax({
			url: '/action.php',
			dataType: 'json',
			type: 'POST',
			data: data,
			success: function(data) {
				console.log(data)
			}
		});
	}

	$.ajax({
		url: '/action.php?action=getSeq',
		success: renderSeq,
		dataType: 'json'
	});

	function renderSeq(data) {
		seq = data;
		var item = '';
		$('.common-list').empty();
		for (var i = 0; i < data.length; i++) {
			item = '<div class="common-list__item">' + renderMarks(data[i]) + data[i].question + '</div>';
			$('.common-list').append(item);
		}

		if (data.length == 0) {
			$('.common-list').append('Нет слов для повторения');
		}

	}

	function renderMarks(item) {
		var marks = '';

		if (item.r1 == 1) {
			marks += '<span class="common-list__mark common-list__mark_green"></span>';
		} else {
			marks += '<span class="common-list__mark common-list__mark_red"></span>';
		}

		if (item.r2 == 1) {
			marks += '<span class="common-list__mark common-list__mark_green"></span>';
		} else {
			marks += '<span class="common-list__mark common-list__mark_red"></span>';
		}

		if (item.r3 == 1) {
			marks += '<span class="common-list__mark common-list__mark_green"></span>';
		} else {
			marks += '<span class="common-list__mark common-list__mark_red"></span>';
		}

		if (item.r4 == 1) {
			marks += '<span class="common-list__mark common-list__mark_green"></span>';
		} else {
			marks += '<span class="common-list__mark common-list__mark_red"></span>';
		}

		if (item.r5 == 1) {
			marks += '<span class="common-list__mark common-list__mark_green"></span>';
		} else {
			marks += '<span class="common-list__mark common-list__mark_red"></span>';
		}

		if (item.r6 == 1) {
			marks += '<span class="common-list__mark common-list__mark_green"></span>';
		} else {
			marks += '<span class="common-list__mark common-list__mark_red"></span>';
		}

		marks = '<div class="common-list__marks">' + marks + '</div>';

		return marks;
	}

	function showRepeatForm() {
		$('.repeat-form').css('display', 'flex');
		showSeq();
	}

	function closeRepeatForm() {
		$('.repeat-form').hide();
	}

	function showSeq() {
		$('.repeat-form__note').empty().hide();
		$('.reapeat-form__question').html(seq[currSeq].question);
		$('.repeat-form__hint').html(seq[currSeq].answer);
	}

	function checkSeq() {
		var answerOrigin = seq[currSeq].answer;
		var answer = $('.repeat-form__textarea_answer').val();
		var data = {};

		if (answerOrigin.toLowerCase() == answer.toLowerCase()) {
			$('.repeat-form__note').html('Верно');
			data.repeatNumber = seq[currSeq].repeatNumber;
			data.id = seq[currSeq].id;
			data.action = 'repeatSeq';
			$.ajax({
				url: 'action.php',
				dataType: 'json',
				data: data,
				type: 'POST',
				success: nextSeq
			});
		} else {
			$('.repeat-form__note').html('Неверно');
		}

		$('.repeat-form__note').show();
		$('.repeat-form__button_check').hide();
		$('.repeat-form__button_next').show();
	}

	function nextSeq() {
		if (currSeq < seq.length-1) {
			currSeq++;
		} else {

		}
		showSeq();
		$('.repeat-form__button_next').hide();
		$('.repeat-form__button_check').show();
		$('.repeat-form__hint').hide();
	}

	function showHint() {
		$('.repeat-form__hint').show();
	}

})();